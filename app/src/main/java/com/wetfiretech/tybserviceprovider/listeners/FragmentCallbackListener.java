package com.wetfiretech.tybserviceprovider.listeners;

import android.support.v4.app.Fragment;
import android.view.View;

public interface FragmentCallbackListener {

    void showSnack(View callingView, String message);

    void replaceFragment(Fragment fragment, boolean addToBackstack);
}
