package com.wetfiretech.tybserviceprovider.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Mayank Gautam
 *         Created: 18/09/17
 */

@Table(name = "Partner")
public class Partner extends Entity implements Parcelable {

    public static final Creator<Partner> CREATOR = new Creator<Partner>() {
        @Override
        public Partner createFromParcel(Parcel in) {
            return new Partner(in);
        }

        @Override
        public Partner[] newArray(int size) {
            return new Partner[size];
        }
    };

    public Partner(){
        super();
    }

    protected Partner(Parcel in) {
        super(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    @JsonIgnore
    Partner getPartner(int id){
        return new Select().from(this.getClass()).where("eid = ?",id).executeSingle();
    }

    @Override
    public String toString() {
        return name;
    }
}
