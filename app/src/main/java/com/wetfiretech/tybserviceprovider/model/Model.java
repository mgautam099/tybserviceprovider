package com.wetfiretech.tybserviceprovider.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Cache;
import com.activeandroid.annotation.Column;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetfiretech.tybserviceprovider.Utils;

/**
 * @author Mayank Gautam
 *         Created: 01/10/17
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Model extends com.activeandroid.Model implements Parcelable{

    public static final Creator<Model> CREATOR = new Creator<Model>() {
        @Override
        public Model createFromParcel(Parcel in) {
            return new Model(in);
        }

        @Override
        public Model[] newArray(int size) {
            return new Model[size];
        }
    };
    @Column(name = "eid")
    @JsonProperty("id")
    public int eid = -1;
    Model(){
        super();
    }

    protected Model(Parcel in) {
        eid = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(eid);
    }

    @Override
    public int describeContents() {
        return eid;
    }

    public void truncate(Class<? extends Model> type) {
        String tableName = Cache.getTableInfo(type).getTableName();

        try{
            // Delete all rows from table
            ActiveAndroid.execSQL(String.format("DELETE FROM %s;", tableName));

            // Reset ids
            ActiveAndroid.execSQL(String.format("DELETE FROM sqlite_sequence WHERE name='%s';", tableName));
        }
        catch (Exception e){
            Utils.logException(e);
        }

    }

    @JsonProperty("id")
    public int getEid() {
        return eid;
    }

    @JsonProperty("id")
    public void setEid(int eid) {
        this.eid = eid;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Model model = (Model) o;

        return eid == model.eid;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + eid;
        return result;
    }

    public void update(Model model) {

    }
}
