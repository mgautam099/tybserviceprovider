package com.wetfiretech.tybserviceprovider.model;

import android.os.Parcelable;

import com.activeandroid.Cache;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mayank Gautam
 *         Created: 25/10/17
 */

@Table(name = "ServiceProviderPartner")
public class ServiceProviderPartner extends Model implements Parcelable {

    @Column
    public ServiceProvider serviceProvider;

    @Column
    public Partner partner;

    @Column
    public boolean isActive;

    public static List<ServiceProviderPartner> allItems(Partner partner){
        return new Select().from(ServiceProviderPartner.class).where(Cache.getTableName(ServiceProviderPartner.class) + ".partner=?", partner.getId()).execute();
    }


    public static ArrayList<ServiceProvider> getAllServiceProviders(Partner partner){
        ArrayList<ServiceProvider> sps = new ArrayList<>();
        List<ServiceProviderPartner> spps = new Select().from(ServiceProviderPartner.class).where(Cache.getTableName(ServiceProviderPartner.class) + ".partner=?", partner.getId()).execute();
        for(ServiceProviderPartner spp: spps){
            sps.add(spp.serviceProvider);
        }
        return sps;
    }
}
