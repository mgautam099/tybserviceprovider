package com.wetfiretech.tybserviceprovider.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Mayank Gautam
 *         Created: 03/11/17
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Log implements Parcelable {
    public static final Creator<Log> CREATOR = new Creator<Log>() {
        @Override
        public Log createFromParcel(Parcel in) {
            return new Log(in);
        }

        @Override
        public Log[] newArray(int size) {
            return new Log[size];
        }
    };
    @JsonProperty("model_id")
    public int modelId;
    @JsonProperty("entry_table")
    public String entryTable;
    @JsonProperty("timestamp")
    public String timestamp;
    @JsonProperty("sp_user")
    public String spUser;
    @JsonProperty("user")
    public int user;
    @JsonProperty("action")
    public int action;
    @JsonProperty("partner")
    public int partner;
    @JsonProperty("client_user")
    public int clientUser;
    @JsonProperty("id")
    public int id;

    public Log(){

    }

    protected Log(Parcel in) {
        modelId = in.readInt();
        entryTable = in.readString();
        timestamp = in.readString();
        spUser = in.readString();
        user = in.readInt();
        action = in.readInt();
        partner = in.readInt();
        clientUser = in.readInt();
        id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(modelId);
        dest.writeString(entryTable);
        dest.writeString(timestamp);
        dest.writeString(spUser);
        dest.writeInt(user);
        dest.writeInt(action);
        dest.writeInt(partner);
        dest.writeInt(clientUser);
        dest.writeInt(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
