package com.wetfiretech.tybserviceprovider.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Table;

/**
 * @author Mayank Gautam
 *         Created: 03/10/17
 */

@Table(name = "ServiceType")
public class ServiceType extends Entity implements Parcelable {

    public ServiceType(){
        super();
    }

    protected ServiceType(Parcel in) {
        super(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    public static final Creator<ServiceType> CREATOR = new Creator<ServiceType>() {
        @Override
        public ServiceType createFromParcel(Parcel in) {
            return new ServiceType(in);
        }

        @Override
        public ServiceType[] newArray(int size) {
            return new ServiceType[size];
        }
    };
}
