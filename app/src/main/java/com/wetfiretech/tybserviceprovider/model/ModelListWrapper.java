package com.wetfiretech.tybserviceprovider.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author Mayank Gautam
 *         Created: 18/09/17
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ModelListWrapper<T extends Model> {

    @JsonProperty("objects")
    public List<T> objects;
}
