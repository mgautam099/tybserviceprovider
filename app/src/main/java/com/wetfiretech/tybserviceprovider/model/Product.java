package com.wetfiretech.tybserviceprovider.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetfiretech.tybserviceprovider.Utils;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
@Table(name = "Product")
public class Product extends Entity implements Parcelable {

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
    @Column(name = "image")
    @JsonProperty("image")
    public String productImage;
    @Column(name = "resource_uri")
    @JsonProperty("resource_uri")
    public String resourceUri;
    @Column(name = "description")
    @JsonProperty("description")
    public String descr;
    @Column(name = "category")
    private Category category;

    public Product(){
        super();
    }

    protected Product(Parcel in) {
        super(in);
        productImage = in.readString();
        resourceUri = in.readString();
        descr = in.readString();
        category = in.readParcelable(Category.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(productImage);
        dest.writeString(resourceUri);
        dest.writeString(descr);
        dest.writeParcelable(category, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @JsonProperty("category")
    public Category getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(Category category) {
        if(category!=null)
            this.category = Utils.getById(category);
    }


    @JsonProperty("category_id")
    public void setCategoryId(int category) {
        this.category = Utils.getById(Category.class,category);
    }


//    @JsonProperty("partner")
//    public List<Partner> getPartner() {
//        ArrayList<Partner> list = new ArrayList<>();
//        if(partner!=null)
//            list.add(partner);
//        return list;
//    }

    @JsonProperty("partner")
    public void setPartner(List<Partner> partners) {
        for(Partner p: partners){
            ProductPartner pp = new ProductPartner();
            pp.partner = Utils.getById(p);
            if(pp.partner!=null){
                this.save();
                pp.product = this;
                pp.save();
            }
        }
    }

    @JsonProperty("partner_id")
    public void setPartnerId(List<Integer> partners) {
        for(int p: partners){
            ProductPartner pp = new ProductPartner();
            pp.partner = Utils.getById(Partner.class,p);
            if(pp.partner!=null){
                this.save();
                pp.product = this;
                pp.save();
            }
        }
    }


    //    public String getProductID() {
//        return productID;
//    }
//
//    public void setProductID(String productID) {
//        this.productID = productID+1;
//    }

    @JsonIgnore
    public List<Product> getAllProducts() {
        return new Select().from(Product.class).execute();
    }

    @JsonIgnore
    public String[] getProductNames(List<Product> products) {
        String[] productNames = new String[products.size()];
        for (int i=0; i<productNames.length; i++) {
            productNames[i] = products.get(i).name;
        }
        return productNames;
    }

    @Override
    public String toString() {
        return name;
    }
}
