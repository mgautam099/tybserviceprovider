package com.wetfiretech.tybserviceprovider.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Table;

/**
 * @author Mayank Gautam
 *         Created: 18/09/17
 */

@Table(name = "Category")
public class Category extends Entity implements Parcelable {

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public Category(){
        super();
    }

    protected Category(Parcel in) {
        super(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

}
