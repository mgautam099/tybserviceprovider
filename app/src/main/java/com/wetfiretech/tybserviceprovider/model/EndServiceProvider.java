package com.wetfiretech.tybserviceprovider.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetfiretech.tybserviceprovider.Utils;

/**
 * @author Mayank Gautam
 *         Created: 29/09/17
 */
@Table(name = "EndServiceProvider")
public class EndServiceProvider extends Person implements Parcelable {

    public static final Creator<EndServiceProvider> CREATOR = new Creator<EndServiceProvider>() {
        @Override
        public EndServiceProvider createFromParcel(Parcel in) {
            return new EndServiceProvider(in);
        }

        @Override
        public EndServiceProvider[] newArray(int size) {
            return new EndServiceProvider[size];
        }
    };
    @Column
    private Partner partner;
    @Column
    private ServiceProvider serviceProvider;

    public EndServiceProvider(){
        super();
    }

    protected EndServiceProvider(Parcel in) {
        super(in);
        partner = in.readParcelable(Partner.class.getClassLoader());
        serviceProvider = in.readParcelable(ServiceProvider.class.getClassLoader());
    }

    public static EndServiceProvider getByPhone(String phone){
        return new Select().from(EndServiceProvider.class).where("phone_no = ?",phone).executeSingle();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(partner, flags);
        dest.writeParcelable(serviceProvider, flags);
    }

    @JsonProperty("partner")
    public Partner getPartner() {
        return partner;
    }

    @JsonProperty("partner")
    public void setPartner(Partner partner) {
        this.partner = Utils.getById(Partner.class,partner.eid);
    }

    @JsonProperty("partner_id")
    public void setPartnerId(int partner) {
        this.partner = Utils.getById(Partner.class,partner);
    }

    @JsonProperty("service_provider")
    public ServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    @JsonProperty("service_provider")
    public void setServiceProvider(ServiceProvider serviceProvider) {
        this.serviceProvider = Utils.getById(ServiceProvider.class,serviceProvider.eid);
    }

    @JsonProperty("service_provider_id")
    public void setService_ProviderId(int s) {
        serviceProvider = Utils.getById(ServiceProvider.class,s);
    }
}
