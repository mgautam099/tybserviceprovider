
package com.wetfiretech.tybserviceprovider.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Table(name = "PaymentModes")
public class PaymentMode extends Model implements Parcelable{

    public static final Creator<PaymentMode> CREATOR = new Creator<PaymentMode>() {
        @Override
        public PaymentMode createFromParcel(Parcel in) {
            return new PaymentMode(in);
        }

        @Override
        public PaymentMode[] newArray(int size) {
            return new PaymentMode[size];
        }
    };
    @Column(name = "mode")
    @JsonProperty("mode")
    private String mode;

    public PaymentMode(){
        super();
    }

    protected PaymentMode(Parcel in) {
        super(in);
        mode = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(mode);
    }


    @JsonProperty("mode")
    public String getMode() {
        return mode;
    }

    @JsonProperty("mode")
    public void setMode(String mode) {
        this.mode = mode;
    }

    @JsonIgnore
    public PaymentMode getPaymentMode(int id){
        return new Select().from(this.getClass()).where("eid = ?", id).executeSingle();
    }

}
