package com.wetfiretech.tybserviceprovider.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.fasterxml.jackson.annotation.JsonProperty;

@Table(name = "State")
public class State extends Model implements Parcelable {


    public static final Creator<State> CREATOR = new Creator<State>() {
        @Override
        public State createFromParcel(Parcel in) {
            return new State(in);
        }

        @Override
        public State[] newArray(int size) {
            return new State[size];
        }
    };
    //@Column
    //@JsonProperty("country")
    //private Country country;
    @Column(name = "state_name")
    @JsonProperty("state_name")
    private String stateName;

    public State(){
        super();
    }

    protected State(Parcel in) {
        super(in);
        stateName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(stateName);
    }

//    @JsonProperty("country")
//    public Country getCountry() {
//        return country;
//    }
//
//    @JsonProperty("country")
//    public void setCountry(Country country) {
//
//        country.save();
//        this.country = country;
//    }

    @JsonProperty("state_name")
    public String getStateName() {
        return stateName;
    }

    @JsonProperty("state_name")
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @Override
    public String toString() {
        return stateName;
    }
}
