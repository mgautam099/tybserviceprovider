package com.wetfiretech.tybserviceprovider.model;

import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetfiretech.tybserviceprovider.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mayank Gautam
 *         Created: 03/10/17
 */
@Table(name = "ServiceProvider")
public class ServiceProvider extends Person implements Parcelable {

    public ServiceProvider(){
        super();
    }
    protected ServiceProvider(Parcel in) {
        super(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    public static final Creator<ServiceProvider> CREATOR = new Creator<ServiceProvider>() {
        @Override
        public ServiceProvider createFromParcel(Parcel in) {
            return new ServiceProvider(in);
        }

        @Override
        public ServiceProvider[] newArray(int size) {
            return new ServiceProvider[size];
        }
    };

    @JsonIgnore
    public static ServiceProvider getServiceProvider(int id){
        return Utils.getById(ServiceProvider.class,id);
    }

    @JsonIgnore
    public static ArrayList<ServiceProvider> getAllServiceProviders(){
        return new ArrayList<>(Utils.getAll(ServiceProvider.class));
    }
    @JsonProperty("partner")
    public void setPartner(List<Partner> partner){
        for(Partner p: partner){
            ServiceProviderPartner spp = new ServiceProviderPartner();
            spp.partner = Utils.getById(p);
            if(spp.partner!=null){
                this.save();
                spp.serviceProvider = this;
                spp.save();
            }
        }
    }

    @JsonProperty("partner_id")
    public void setPartnerId(List<Integer> partners) {
        for(int p: partners){
            ServiceProviderPartner spp = new ServiceProviderPartner();
            spp.partner = Utils.getById(Partner.class,p);
            if(spp.partner!=null){
                this.save();
                spp.serviceProvider = this;
                spp.save();
            }
        }
    }


    public void saveToPrefs(SharedPreferences.Editor editor) {
        super.saveToPrefs(editor);
    }

    public void restorePrefs(SharedPreferences sharedPrefs) {
        super.restorePrefs(sharedPrefs);
    }
}
