package com.wetfiretech.tybserviceprovider.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetfiretech.tybserviceprovider.Utils;

/**
 * @author Mayank Gautam
 *         Created: 03/10/17
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginDetail implements Parcelable {

    @JsonProperty("AccessToken")
    public String access_token;
    @JsonProperty("timestamp")
    public String timestamp;
    @JsonProperty("token_type")
    public String token_type;
    @JsonProperty("user_id")
    public int userId;
    @JsonProperty("role")
    public String role;
    public String username;
    @JsonProperty("sp")
    public ServiceProvider sp;

    public LoginDetail(){
        super();
    }

    protected LoginDetail(Parcel in) {
        access_token = in.readString();
        timestamp = in.readString();
        token_type = in.readString();
        userId = in.readInt();
        role = in.readString();
        username = in.readString();
        sp = in.readParcelable(ServiceProvider.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(access_token);
        dest.writeString(timestamp);
        dest.writeString(token_type);
        dest.writeInt(userId);
        dest.writeString(role);
        dest.writeString(username);
        dest.writeParcelable(sp, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LoginDetail> CREATOR = new Creator<LoginDetail>() {
        @Override
        public LoginDetail createFromParcel(Parcel in) {
            return new LoginDetail(in);
        }

        @Override
        public LoginDetail[] newArray(int size) {
            return new LoginDetail[size];
        }
    };

    public void saveToPrefs(SharedPreferences.Editor editor) {
        editor.putString("AccessToken",access_token);
        editor.putString("token_type",token_type);
        editor.putString("timestamp",timestamp);
        editor.putInt("user_id",userId);
        editor.putString("role",role);
        editor.putString("username",username);
        sp.saveToPrefs(editor);
    }

    public void updateTimestamp(String timestamp, Context context){
        this.timestamp = timestamp;
        Utils.saveToPrefs("timestamp",timestamp, context);
    }

    public void restorePrefs(SharedPreferences sharedPrefs){
        access_token = sharedPrefs.getString("AccessToken","no-token");
        username = sharedPrefs.getString("token_type","00000");
        timestamp = sharedPrefs.getString("timestamp","2017-11-10 13:28:29");
        userId =  sharedPrefs.getInt("user_id",-1);
        role = sharedPrefs.getString("role","");
        username = sharedPrefs.getString("username","");
        sp = new ServiceProvider();
        sp.restorePrefs(sharedPrefs);
    }
}
