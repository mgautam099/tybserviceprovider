package com.wetfiretech.tybserviceprovider.network

import android.content.Context
import com.activeandroid.ActiveAndroid
import com.activeandroid.Cache
import com.fasterxml.jackson.databind.ObjectMapper
import com.wetfiretech.tybserviceprovider.OnLoadFinishListener
import com.wetfiretech.tybserviceprovider.Utils
import com.wetfiretech.tybserviceprovider.Utils.*
import com.wetfiretech.tybserviceprovider.model.*
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.lang.StringBuilder
//import java.util.*
import kotlin.collections.ArrayList

object NetworkRequest {
    var loginDetail: LoginDetail? = null

    private fun <T> getService(service: Class<T>, context: Context): T? {
        return RetrofitClient.getClient(Utils.getBaseUrl(context), context).create(service)
    }

//    private val KEY_USERNAME  = "username"
//    private val KEY_PASSWORD  = "password"
//    private val KEY_REGISTRATION  = "registration"
//    private val KEY_APIKEY  = "ApiKey"
//    private val KEY_TIMESTAMP  = "timestamp"

    fun generateNewToken(username: String, password: String,registrationToken: String, listener: OnLoadFinishListener) {
//        listener.showProgress()
        listener.showProgress("Logging in")
        val apiService = getService(ApiService::class.java, listener.getContext())
        val fields = HashMap<String, String>()
        fields.put(KEY_USERNAME, username)
        fields.put(KEY_PASSWORD, password)
        fields.put(KEY_REGISTRATION,registrationToken)
        apiService!!.login(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ c ->
                    c.username = username
                    loginDetail = c
                    listener.saveToken(c)}, listener::onError)
    }

    fun register(registerDetails: HashMap<String, String>, listener: OnLoadFinishListener) {
        listener.showProgress("Registering User")
        val apiService = getService(ApiService::class.java, listener.getContext())
        apiService!!.register(registerDetails)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ c ->
                    c.username = registerDetails[KEY_USERNAME]
                    loginDetail = c
                    listener.saveToken(c)
                }, listener::onError)
    }

    fun requestOrders(listener: OnLoadFinishListener) {
        val accessToken = "ApiKey " + loginDetail!!.username!! +":"+ loginDetail!!.access_token!!
        val apiService = getService(ApiService::class.java, listener.getContext())
        apiService!!.getPartners(accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .doOnNext(this::saveModels)
                .flatMap { apiService.getStates(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getPincodes(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getProducts(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getServiceTypes(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getClients(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getCustomers(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getPaymentModes(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getEndServiceProviders(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getOrders(accessToken) }
                .doOnNext(this::saveModels)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({listener.displayHomeFragment()}, listener::onError)
    }

    fun updateOrder(orderDetail: OrderDetail, listener: OnLoadFinishListener){
        val accessToken = "ApiKey " + loginDetail!!.username!! +":"+ loginDetail!!.access_token!!
        val apiService = getService(ApiService::class.java, listener.getContext())
        apiService!!.putOrder(orderDetail.eid,accessToken, orderDetail)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .doOnNext { t-> saveModel(t, orderDetail) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({
                    listener.onLoadComplete(orderDetail,"OrderDetail "+ orderDetail.complain_no+" Updated")},
                listener::onError)
    }

    fun updateOrders(orders: List<OrderDetail>, listener: OnLoadFinishListener){
        if(loginDetail==null)
            loginDetail = Utils.populateLoginDetails(listener.getContext())
        if(loginDetail==null)
            return
        val accessToken = "ApiKey " + loginDetail!!.username!! +":"+ loginDetail!!.access_token!!
        val apiService = getService(ApiService::class.java, listener.getContext())
        val objects = ArrayList<Observable<OrderDetail>>()
        orders.mapTo(objects) {
            apiService!!.putOrder(it.eid,accessToken, it)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(Schedulers.io())
                    .doOnNext { t-> saveModel(t, it) }
        }

        Observable.zip(objects, {})
                .subscribeOn(Schedulers.newThread())
                .subscribe({listener.onLoadComplete(Utils.RESULT_UPDATE_COMPLETE,"")}, listener::onError)

    }

    fun getOrder(order_id: String, listener: OnLoadFinishListener){
        if(loginDetail==null)
            loginDetail = Utils.populateLoginDetails(listener.getContext())
        if(loginDetail==null)
            return
        val accessToken = "ApiKey " + loginDetail!!.username!! +":"+ loginDetail!!.access_token!!
        val apiService = getService(ApiService::class.java, listener.getContext())
        apiService!!.getOrder(order_id,accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .doOnNext (this::saveOrder)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                        {order -> listener.onLoadComplete(order,"Order Updated")},
                        listener::onError)
    }

    fun getLog(listener: OnLoadFinishListener){
        if(loginDetail==null)
            loginDetail = Utils.populateLoginDetails(listener.getContext())
        if(loginDetail==null)
            return
        val loginDetail = loginDetail
        val fields = HashMap<String, String>()
        fields.put(KEY_APIKEY, loginDetail!!.access_token)
        fields.put(KEY_TIMESTAMP, loginDetail.timestamp)
        val apiService = getService(ApiService::class.java, listener.getContext())
        apiService!!.getLog(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .doOnNext ({r -> saveLog(r, listener.getContext())})
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                        {listener.onLoadComplete(Utils.RESULT_SYNC_COMPLETE,"Sync complete")},
                        listener::onError)
    }

    private fun saveLog(logs: List<LogWrapper>, context: Context) {
//        val rClient = RetrofitClient.getClient(Utils.getBaseUrl(listener.getContext()),listener.getContext())
        var timestamp:String? = null
        var str = StringBuilder(Utils.getFromPrefs(Utils.KEY_UPDATED_MODELS,context))
        if(str.isNotEmpty())
            str.append(",")
        val objectMapper = ObjectMapper()
        ActiveAndroid.beginTransaction()
        try {
            for(log in logs){
                saveOrDeleteModel(log,objectMapper)
                timestamp = log.log.timestamp
                if(log.log.action!=-1 && log.log.entryTable==Cache.getTableName(OrderDetail::class.java))
                    str.append(log.log.modelId).append(",")
            }
            ActiveAndroid.setTransactionSuccessful()
        } catch (e: Exception){
            Utils.logException(e)
        }
        finally {
            ActiveAndroid.endTransaction()
            if(timestamp!=null)
                loginDetail?.updateTimestamp(timestamp,context)
            if(!str.isEmpty())
                Utils.saveToPrefs(Utils.KEY_UPDATED_MODELS,str.substring(0,str.length-1),context)
        }
    }

    private fun saveOrDeleteModel(log: LogWrapper, objectMapper: ObjectMapper) {
        val c = Cache.getTable(log.log.entryTable)

        when(log.log.action){
            0,1 -> {
                var model = if(log.log.entryTable == Cache.getTableName(OrderDetail::class.java))
                    Utils.getByTimestampModel(c,log.data.get("timestamp").asText())
                else
                    Utils.getByIdModel(c,log.log.modelId)
                model = if(model!=null)
                            objectMapper.readerForUpdating(model).readValue(log.data)
                        else
                            objectMapper.treeToValue(log.data, c)
                model.save()

            }
            -1 -> Utils.getByIdModel(c,log.log.modelId)?.delete()
        }
    }

    private fun saveOrder(orderDetail: OrderDetail) {
        var order1 = Utils.getById(orderDetail.javaClass, orderDetail.eid)
        if(order1!=null)
        {
            var esp = Utils.getById(EndServiceProvider::class.java, orderDetail.end_service_provider.eid)
            if(esp==null)
                orderDetail.end_service_provider.save()
            esp = Utils.getById(EndServiceProvider::class.java, orderDetail.end_service_provider.eid)
            order1.end_service_provider = esp
            order1.paidAmount = orderDetail.paidAmount
            order1.orderStatus = orderDetail.orderStatus
            order1.complain_resolve_comment = orderDetail.complain_resolve_comment
            order1.setService_Provider(orderDetail.service_provider)
            order1.setCustomer(orderDetail.customer)
            order1.setProduct(orderDetail.product)
            order1.in_Warranty = orderDetail.in_Warranty
            order1.issue_description = orderDetail.issue_description
            try {
                order1.save()
            }
            catch (e:Exception){
                Utils.logException(e)
            }
        }
        else{
            try{
                orderDetail.save()
            }
            catch (e: Exception){
                Utils.logException(e)
            }
        }

    }

    private fun saveModel(model2: OrderDetail, model: OrderDetail){
        model.eid = model2.eid
        model.customer?.eid = model2.customer?.eid!!
        model.customer?.save()
        model.end_service_provider?.eid = model2.end_service_provider?.eid
        model.end_service_provider?.save()
        model.setClient(model2.client)
        model.updateType = OrderDetail.OrderUpdateType.NO_UPDATE
        try{
            model.save()
        }
        catch(e: Exception){
            Utils.logException(e)
        }
    }

    private fun saveModels(models: List<Model>) {
        ActiveAndroid.beginTransaction()
        try {
            for (model in models) {
                model.save()
            }
            ActiveAndroid.setTransactionSuccessful()
        } finally {
            ActiveAndroid.endTransaction()
        }
    }

    private fun saveModels(models: ModelListWrapper<out Model>) {
        saveModels(models.objects)
    }
}
