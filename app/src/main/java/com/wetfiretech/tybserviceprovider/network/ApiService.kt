package com.wetfiretech.tybserviceprovider.network

import com.wetfiretech.tybserviceprovider.model.*
import retrofit2.http.*
import rx.Observable

interface ApiService {
    @POST("sp/register/")
    @FormUrlEncoded
    fun register(@FieldMap fieldMap: Map<String, String>): Observable<LoginDetail>

    @POST("sp/login")
    @FormUrlEncoded
    fun login(@FieldMap fieldMap: Map<String, String>): Observable<LoginDetail>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("api/v1/partner/")
    fun getPartners(@Header("Authorization") token: String): Observable<ModelListWrapper<Partner>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("api/v1/state/")
    fun getStates(@Header("Authorization") token: String): Observable<ModelListWrapper<State>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("api/v1/pincode/")
    fun getPincodes(@Header("Authorization") token: String): Observable<ModelListWrapper<Pincode>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("api/v1/product/")
    fun getProducts(@Header("Authorization") token: String): Observable<ModelListWrapper<Product>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("api/v1/client/")
    fun getClients(@Header("Authorization") token: String): Observable<ModelListWrapper<Client>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("api/v1/service_type/")
    fun getServiceTypes(@Header("Authorization") token: String): Observable<ModelListWrapper<ServiceType>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("api/v1/order_detail/")
    fun getOrders(@Header("Authorization") token: String): Observable<ModelListWrapper<OrderDetail>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("api/v1/customer/")
    fun getCustomers(@Header("Authorization") token: String): Observable<ModelListWrapper<Customer>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("api/v1/payment_mode/")
    fun getPaymentModes(@Header("Authorization") token: String): Observable<ModelListWrapper<PaymentMode>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("api/v1/end_service_provider/")
    fun getEndServiceProviders(@Header("Authorization") token: String): Observable<ModelListWrapper<EndServiceProvider>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @PUT("api/v1/order_detail/{order_id}/")
    fun putOrder(@Path("order_id") order_id: Int, @Header("Authorization") token: String, @Body orderDetail: OrderDetail): Observable<OrderDetail>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("api/v1/order_detail/{order_id}/")
    fun getOrder(@Path("order_id") order_id: String, @Header("Authorization") token: String): Observable<OrderDetail>

    @POST("get_log/")
    @FormUrlEncoded
    fun getLog(@FieldMap fieldMap: Map<String, String>): Observable<List<LogWrapper>>
}