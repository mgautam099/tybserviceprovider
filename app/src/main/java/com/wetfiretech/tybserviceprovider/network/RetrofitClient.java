package com.wetfiretech.tybserviceprovider.network;

import android.content.Context;
import android.os.Build;

import com.wetfiretech.tybserviceprovider.BuildConfig;
import com.wetfiretech.tybserviceprovider.OnLoadFinishListener;
import com.wetfiretech.tybserviceprovider.R;
import com.wetfiretech.tybserviceprovider.Utils;
import com.wetfiretech.tybserviceprovider.model.LoginDetail;
import com.wetfiretech.tybserviceprovider.model.OrderDetail;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit.converter.jackson.JacksonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.functions.FuncN;
import rx.schedulers.Schedulers;


public class RetrofitClient {
    private static Retrofit retrofit = null;

    public static Retrofit getClient(String baseUrl, final Context context) {
        if (retrofit == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            OkHttpClient client;
            builder.connectTimeout(30, TimeUnit.SECONDS);
            builder.addInterceptor(chain -> {
                Request original = chain.request();
                Request newRequest = original.newBuilder()
                        .header("User-Agent", context.getResources().getString(R.string.app_name) + "/" + BuildConfig.VERSION_NAME)
                        .addHeader("Application", BuildConfig.APPLICATION_ID)
                        .addHeader("Client", Utils.getAndroidVersion())
                        .addHeader("Version", BuildConfig.VERSION_NAME)
                        .addHeader("Device", Build.BRAND + " " + Build.MODEL)
                        .build();
                return chain.proceed(newRequest);
            });
            client = builder.build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(client)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(JacksonConverterFactory.create())
                    .build();

        }
        return retrofit;
    }

    private <T> T getService(Class<T> service,Context context){
        return RetrofitClient.getClient(Utils.getBaseUrl(context), context).create(service);
    }

    void updateOrders(List<OrderDetail> orders, OnLoadFinishListener listener){
        if(NetworkRequest.INSTANCE.getLoginDetail()==null)
            NetworkRequest.INSTANCE.setLoginDetail(Utils.populateLoginDetails(listener.getContext()));
        if(NetworkRequest.INSTANCE.getLoginDetail()==null)
            return;
        LoginDetail loginDetail = NetworkRequest.INSTANCE.getLoginDetail();
        String accessToken = "ApiKey " + loginDetail.username +":"+ loginDetail.access_token;
        ApiService apiService = getService(ApiService.class, listener.getContext());
        ArrayList<Observable<OrderDetail>> objects = new ArrayList<>();
        for (OrderDetail it : orders) {
            objects.add(apiService.putOrder(it.eid,accessToken, it)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(Schedulers.computation()));
        }
        Observable.zip(objects, args -> {
            List<OrderDetail> updatedOrders = new ArrayList<>();
            for (Object arg: args) {
                if(arg instanceof OrderDetail){
                    updatedOrders.add((OrderDetail) arg);
                }
            }
            return updatedOrders;
        })
                .subscribeOn(Schedulers.newThread())
                .subscribe(updatedOrders -> { for(OrderDetail order: updatedOrders) System.out.println(order.eid);}, listener::onError);

    }


    public static void resetService(){
        retrofit = null;
    }
}
