package com.wetfiretech.tybserviceprovider

import android.content.Context

import com.wetfiretech.tybserviceprovider.model.LoginDetail
import com.wetfiretech.tybserviceprovider.model.OrderDetail

interface OnLoadFinishListener {
    fun displayHomeFragment()

    fun onError(throwable: Throwable)

    fun onLoadComplete(result: Any?, message: String)

    fun saveToken(loginDetail: LoginDetail)

    fun getContext(): Context

    fun showProgress(message: String)
    fun hideProgress()
}
