package com.wetfiretech.tybserviceprovider.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author Mayank Gautam
 *         Created: 26/09/17
 */

public class CircleView extends View {

    private int mFillColor = DEFAULT_FILL_COLOR;
    private static final int DEFAULT_FILL_COLOR = Color.RED;
    private Paint mFillPaint;

    public CircleView(Context context) {
        super(context);
        init();
    }

    public CircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        mFillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFillPaint.setStyle(Paint.Style.FILL);
        mFillPaint.setColor(mFillColor);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(canvas.getWidth()/2, canvas.getWidth()/2, radius, mFillPaint);
    }

    public void setColor(@ColorInt int color){
        mFillColor = color;
        mFillPaint.setColor(mFillColor);
        invalidate();
    }

    float radius;
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        radius = w/2;
    }
}
