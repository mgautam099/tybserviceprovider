package com.wetfiretech.tybserviceprovider.custom;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

public class SquaredImageView extends AppCompatImageView {
    public SquaredImageView(Context context) {
        super(context);
        initView();
    }

    public SquaredImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public SquaredImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            ColorStateList tintList = getImageTintList();
            if(tintList == null){
                return;
            }
            setColorFilter(tintList.getDefaultColor(), PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int w = getMeasuredWidth();
        int h = getMeasuredHeight();
        setMeasuredDimension(w>h?w:h, w>h?w:h);
    }


}
