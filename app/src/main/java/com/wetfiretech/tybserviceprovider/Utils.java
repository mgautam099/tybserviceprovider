package com.wetfiretech.tybserviceprovider;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import com.activeandroid.query.Select;
import com.crashlytics.android.Crashlytics;
import com.wetfiretech.tybserviceprovider.model.EndServiceProvider;
import com.wetfiretech.tybserviceprovider.model.LoginDetail;
import com.wetfiretech.tybserviceprovider.model.Model;

import java.lang.reflect.Field;
import java.util.List;

public class Utils {

    public final static String SHARED_PREFS = "ServiceProviderPrefs";
    public static final String KEY_REGISTRATION_TOKEN = "RegistrationToken";
    public static final String NOTIFICATION_INTENT = "SPNotifyOrder";
    public static final String ARGS_EXTRAS = "extras";
    public static final String KEY_ORDER_ID = "order_id";
    public static final String KEY_MODEL_UPDATE = "model_update";
    public static final String KEY_OLD_ACCESS_TOKEN = "access-token";
    public static final String KEY_COMPLAINT_NUM = "complain_num";
    public static final String KEY_NOTIFICATION_MESSAGE = "notification_message";
    public static final String KEY_BASE_URL = "base-url";
    public static final String KEY_UPDATED_MODELS = "updated-models";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_REGISTRATION = "registration";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_NAME = "name";
    public static final String KEY_PARTNER_CODE = "partner_code";
    public static final String KEY_APIKEY = "ApiKey";
    public static final String KEY_TIMESTAMP = "timestamp";


    public static final String DEFAULT_BASE_URL = "http://thankyoubhaiya.com/";

    public static final String RESULT_UPDATE_COMPLETE = "UpdateComplete";
    public static final String RESULT_SYNC_COMPLETE = "SyncComplete";
    private static String BASE_URL;

    public static String getAndroidVersion() {
        StringBuilder builder = new StringBuilder("Android ");
        builder.append(Build.VERSION.RELEASE);
        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field f :
                fields) {
            String fieldName = f.getName();
            int fieldValue = -1;

            try {
                fieldValue = f.getInt(new Object());
            } catch (IllegalAccessException | NullPointerException | IllegalArgumentException e) {
                logException(e);
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append(" ").append(fieldName).append(" v").append(fieldValue);
            }
        }
        return builder.toString();
    }

    public static int getState(String pincode){
        int code =0;
        if(pincode!=null && pincode.length()>=2)
            code = Integer.parseInt(pincode.substring(0,2));
        if(code==11)
            return 0;
        else if(code < 14)
            return 9;
        else if(code < 17)
            return 22;
        else if(code < 18)
            return 10;
        else if(code < 20)
            return 11;
        else if(code < 29)
            return 28;
        else if(code <35)
            return 23;
        else if(code < 40)
            return 8;
        else if(code < 45)
            return 16;
        else if(code < 49)
            return 15;
        else if(code < 50)
            return 6;
        else if(code < 54)
            return -1;
        else if(code <60)
            return 13;
        else if(code < 65)
            return 25;
        else if(code < 70)
            return -1;
        else if(code < 75)
            return -1;
        else if(code < 78)
            return 21;
        else if(code < 79)
            return 4;
        else if(code < 80)
            return -1;
        else
            return -1;
    }

    public static <T extends Model> T getById(Class<T> model, int eid){
        return new Select().from(model).where("eid = ?",eid).executeSingle();
    }

    public static <T extends com.activeandroid.Model> T getByIdModel(Class<T> model, int eid){
        return new Select().from(model).where("eid = ?",eid).executeSingle();
    }

    public static <T extends com.activeandroid.Model> T getByTimestampModel(Class<T> model, String timestamp){
        return new Select().from(model).where("timestamp = ?",timestamp).executeSingle();
    }

    public static <T extends Model> T getById(T model){
        return new Select().from(model.getClass()).where("eid = ?",model.eid).executeSingle();
    }

    public static <T extends Model> T getByField(T model, String field){
        return new Select().from(model.getClass()).where("? = ?",field,model.eid).executeSingle();
    }

    public static <T extends Model> List<T> getAll(Class<T> model){
        return new Select().from(model).execute();
    }

    public static LoginDetail populateLoginDetails(Context context){
        SharedPreferences sharedPrefs = context.getSharedPreferences(Utils.SHARED_PREFS, Context.MODE_PRIVATE);
        if(sharedPrefs.contains("AccessToken")){
            LoginDetail l = new LoginDetail();
            l.restorePrefs(sharedPrefs);
            return l;
        }
        return null;
    }

    public static String getBaseUrl(Context context){
        if(BASE_URL==null){
            SharedPreferences sharedPrefs = context.getSharedPreferences(Utils.SHARED_PREFS, Context.MODE_PRIVATE);
            if(sharedPrefs.contains(KEY_BASE_URL)){
                BASE_URL = sharedPrefs.getString(KEY_BASE_URL,DEFAULT_BASE_URL);
            }
            else{
                BASE_URL = DEFAULT_BASE_URL;
            }
        }
        if(BASE_URL.isEmpty()){
            BASE_URL = DEFAULT_BASE_URL;
        }
        return BASE_URL;
    }

    public static void setBaseUrl(String baseUrl, Context context){
        BASE_URL = baseUrl;
        SharedPreferences sharedPrefs = context.getSharedPreferences(Utils.SHARED_PREFS, Context.MODE_PRIVATE);
        sharedPrefs.edit().putString(KEY_BASE_URL,baseUrl).apply();
    }

    public static void saveToPrefs(String key, String value, Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(Utils.SHARED_PREFS, Context.MODE_PRIVATE);
        sharedPrefs.edit().putString(key,value).apply();
    }

    public static void deleteFromPrefs(String key, Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(Utils.SHARED_PREFS, Context.MODE_PRIVATE);
        sharedPrefs.edit().remove(key).apply();
    }

    public static String getFromPrefs(String key, Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(Utils.SHARED_PREFS, Context.MODE_PRIVATE);
        return sharedPrefs.getString(key,"");
    }

    public static void logException(Throwable throwable) {
        if(BuildConfig.DEBUG)
            throwable.printStackTrace();
        else
            Crashlytics.logException(throwable);
    }

    public static Boolean checkValidPhone(String text) {
        return !(text.isEmpty() || (
                !text.startsWith("9") &&
                        !text.startsWith("8") &&
                        !text.startsWith("7") &&
                        !text.startsWith("6") &&
                        !text.startsWith("0")) || text.length() < 10);
    }

}
