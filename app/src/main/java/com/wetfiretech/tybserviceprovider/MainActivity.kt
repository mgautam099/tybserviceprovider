package com.wetfiretech.tybserviceprovider

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED
import android.support.v4.widget.DrawerLayout.LOCK_MODE_UNDEFINED
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.telephony.SmsManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.activeandroid.Cache
import com.activeandroid.query.Select
import com.crashlytics.android.Crashlytics
import com.wetfiretech.tybserviceprovider.Utils.*
import com.wetfiretech.tybserviceprovider.databinding.NavHeaderMainBinding
import com.wetfiretech.tybserviceprovider.fragment.*
import com.wetfiretech.tybserviceprovider.model.*
import com.wetfiretech.tybserviceprovider.network.NetworkRequest
import com.wetfiretech.tybserviceprovider.network.RetrofitClient
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.placeholder.*
import okhttp3.ResponseBody
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException


class MainActivity : AppCompatActivity(), OnLoadFinishListener,
        OrderListFragment.OnJobListInteractionListener, OrderEntryFragment.OnInteractionListener,
        NavigationView.OnNavigationItemSelectedListener, LoginFragment.LoginFragmentInteractionListener{

    private lateinit var orderDetail: OrderDetail

    private var doubleBackPressed = false

    private var isLoggedIn = false
    private val PERMISSION_REQUEST_SEND_SMS=0

    var notificationReceiver = NotificationReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        NetworkRequest.loginDetail = Utils.populateLoginDetails(this)
        if(NetworkRequest.loginDetail!=null){
            displayHomeFragment()
        }
        else
            initLoginFragment()
        if(getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).contains(KEY_OLD_ACCESS_TOKEN)){
            clearData()
            showSnack(placeholder,"Session expired, please login again!")
        }
        getSMSPermission()
    }

    private fun initNavigationDrawer() {
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        drawer_layout.setDrawerLockMode(LOCK_MODE_UNDEFINED)
        nav_view.setNavigationItemSelectedListener(this)
        nav_view.setCheckedItem(R.id.nav_home)
    }

    private fun bindHeader() {
        val binding :NavHeaderMainBinding = NavHeaderMainBinding.bind(nav_view.getHeaderView(0))
        binding.sp = NetworkRequest.loginDetail?.sp
        val footerText = StringBuilder("v").append(BuildConfig.VERSION_NAME)
        if(Utils.getBaseUrl(this) != Utils.DEFAULT_BASE_URL)
            footerText.append(" - ").append(Utils.getBaseUrl(this))
        nav_footer_version.text = footerText
    }


    private fun initLoginFragment() {
        val fr = LoginFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.fragment, fr)
        fragmentTransaction.commitAllowingStateLoss()
        drawer_layout.setDrawerLockMode(LOCK_MODE_LOCKED_CLOSED)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
//        menu.findItem(R.id.action_logout).isVisible = isLoggedIn
        menu.findItem(R.id.action_sync).isVisible = isLoggedIn
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
//            R.id.action_logout -> {logout()
//                true}
            R.id.action_sync -> {syncAll()
                true}
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun replaceFragment(fragment: Fragment, addToBackStack: Boolean) {
        hideKeyboard(this)
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment, fragment)
        if (addToBackStack)
            fragmentTransaction.addToBackStack(fragment.toString())
        fragmentTransaction.commitAllowingStateLoss()
    }

    fun getCurrentFragment(): Fragment {
        return supportFragmentManager.findFragmentById(R.id.fragment)
    }

    private fun showSnack(view: View, message: String) {
        try {
            Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        } catch (ex: Exception) {
            Utils.logException(ex)
        }

    }

    override fun getContext(): Context {
        return this
    }


    override fun displayHomeFragment() {
        if(!isLoggedIn)
            initHome()
        val fr = OrderListFragment()
        replaceFragment(fr,false)
    }

    private fun getOrders() : List<OrderDetail>?{
        return Select().from(OrderDetail::class.java).orderBy("id DESC").execute() ?: arrayListOf()

    }

    private val SUCCESS_MSG_LOGIN: String = "Successfully logged in!"

    private fun successfulLogin(result: LoginDetail) {
        NetworkRequest.loginDetail = result
        showSnack(findViewById(R.id.placeholder), SUCCESS_MSG_LOGIN)
        replaceFragment(MainActivityFragment(),false)
        NetworkRequest.requestOrders(this)
    }

    private fun initHome(){
        initNavigationDrawer()
        bindHeader()
        isLoggedIn = true
        invalidateOptionsMenu()
    }

    override fun onOrderClicked(item: OrderDetail) {
        discardData = false
        orderDetail = item
        val fr = OrderEntryFragment.newInstance(orderDetail)
        replaceFragment(fr,true)
    }

    private fun truncate(models: List<Model>){
        for(model in models){
            model.truncate(model::class.java)
        }

    }

    private fun clearData(){
        val databaseNamev1 = "Application.db"
        val databaseNamev2 ="TYBServiceProviderDB"
        Cache.getTables()
        val arr = arrayListOf(OrderDetail(),Customer(),Client(),EUser(),Field(),EndServiceProvider(),ServiceProviderPartner(),ServiceProvider(),ProductPartner(),Product(),PaymentMode(),ServiceType(),Category(),Pincode(), State(),Country(),Partner())
        truncate(arr)
        val isDeleted = deleteDatabase(databaseNamev1)
        var rg = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).getString(KEY_REGISTRATION_TOKEN,"")
        var url = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).getString(KEY_BASE_URL, DEFAULT_BASE_URL)
        getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).edit().clear().apply()
        getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).edit().putString(KEY_REGISTRATION_TOKEN,rg).putString(KEY_BASE_URL,url).apply()
    }

    fun restartActivity(){
        clearData()
        val intent = intent
        finish()
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    fun logout() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Would you like to logout?")
        builder.setPositiveButton("Yes", { _, _ ->
            restartActivity()
        })
        builder.setNegativeButton("No", null)
        val appCompatDialog = builder.create()
        appCompatDialog.show()
    }

    var discardData = false

    override fun onBackPressed() {
        if(closeNavigationDrawerIfOpen())
            return

        if(checkIfOrderFragmentDirty()){
            return
        }

        if(checkIfLastFragment())
            return

        super.onBackPressed()
    }

    private fun checkIfOrderFragmentDirty(): Boolean {
        if(getCurrentFragment() is OrderEntryFragment && !discardData){
            if((getCurrentFragment() as OrderEntryFragment).isDirty()){
                AlertDialog.Builder(this).setMessage("Edited data will be discarded?")
                        .setPositiveButton("Yes", { _, _ ->    discardData = true
                            onBackPressed()})
                        .setNegativeButton("No", { _, _ ->     discardData = false})
                        .show()
                return true
            }
        }
        return false
    }

    private fun closeNavigationDrawerIfOpen(): Boolean{
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
            return true
        }
        return false
    }

    private fun checkIfLastFragment(): Boolean{
        if (supportFragmentManager.backStackEntryCount == 0) {
            if (!doubleBackPressed) {
                showSnack(findViewById(R.id.placeholder),"Press back again to exit")
                doubleBackPressed = true
                Handler().postDelayed({ doubleBackPressed = false }, 3000)
                return true
            }
        }
        return false
    }

    fun updateList(orderDetail: OrderDetail?){
        for(fr in supportFragmentManager.fragments){
            if(fr is OrderListFragment){
                fr.addOrder(orderDetail)
                return
            }
        }
    }

    fun syncAll(){
        val syncList = Select().from(OrderDetail::class.java).where("update_type != ?", OrderDetail.OrderUpdateType.NO_UPDATE).execute<OrderDetail>()
        NetworkRequest.updateOrders(syncList,this)
        NetworkRequest.getLog(this)
    }

    override fun onSaveRequested(orderDetail: OrderDetail) {
        orderDetail.end_service_provider?.save()
        orderDetail.save()
        NetworkRequest.updateOrder(orderDetail,this)
        showSnack(placeholder,"Updating Order")
        discardData = true
        if(supportFragmentManager.popBackStackImmediate()){
            (getCurrentFragment() as OrderListFragment).updateOrder(orderDetail)
        }
        if(orderDetail.orderStatusEnum == OrderDetail.OrderStatus.ASSIGNED && orderDetail.paidAmount<1){
            sendSMS(orderDetail.end_service_provider?.phone_no, getString(R.string.service_message, orderDetail.complain_no, orderDetail.customer?.name, orderDetail.customer?.phone_no, orderDetail.product?.name, orderDetail.issue_description, orderDetail.customer?.address))
            sendSMS(orderDetail.customer?.phone_no, getString(R.string.customer_message, orderDetail.complain_no, orderDetail.product?.name, orderDetail.end_service_provider?.phone_no))
        }
    }

    private fun sendSMS(phone_no: String?, message: String) {
        var smsManager = SmsManager.getDefault()
        smsManager.sendTextMessage(phone_no, null, message, null, null)

    }


    class NotificationReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if(intent?.action == Utils.NOTIFICATION_INTENT){
                val bundle = intent.extras?.getBundle(Utils.ARGS_EXTRAS)
                val messageId = bundle?.getInt(Utils.KEY_ORDER_ID)
                val message: String
                val result: Any?
                result = if (messageId == -1) {
                    message = bundle.getString(KEY_MODEL_UPDATE)
                } else {
                    val re: OrderDetail = bundle?.getParcelable(KEY_MODEL_UPDATE)!!
                    message = "Order no " + re.complain_no + " has been updated"
                    re.updateType = OrderDetail.OrderUpdateType.POST
                    re
                }
                (context as? MainActivity)?.onLoadComplete(result, message)
            }
        }
    }

    var progressView: ProgressDialog? = null
    override fun showProgress(message: String) {
        if(progressView == null)
        {
            progressView = ProgressDialog(this)
            progressView?.isIndeterminate = true
            progressView?.setMessage("Logging in")
            progressView?.setCancelable(false)
        }
        progressView?.show()
    }

    override fun hideProgress() {
        progressView?.dismiss()
        progressView = null
    }


    protected fun getSMSPermission() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {
                AlertDialog.Builder(this).setMessage("Please grant SMS Permission as it is needed for the functioning of app!").setNeutralButton("OK",{_,_ -> ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.SEND_SMS),
                        PERMISSION_REQUEST_SEND_SMS)}).show()
            } else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.SEND_SMS),
                        PERMISSION_REQUEST_SEND_SMS)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(requestCode == PERMISSION_REQUEST_SEND_SMS){
            if (grantResults.isNotEmpty()
               && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                if(!ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.SEND_SMS))
                {
                    AlertDialog.Builder(this).setMessage("This app need SMS permission to run, please provide the permission. App will now exit!")
                            .setNeutralButton("OK", {_,_ -> finish()}).setCancelable(false).show()
                }
                else{
                    getSMSPermission()
                }
            }
        }
    }

    override fun onError(throwable: Throwable) {
        try{
            hideProgress()
            Utils.logException(throwable)
            if(getCurrentFragment() is MainActivityFragment){
                restartActivity()
            }
            var ne = getErrorBody(throwable)
            if(getCurrentFragment() is LoginFragment && ne.descr==null && ne.code==403){
                ne.descr = "Invalid username/password"
            }
            if(ne.descr==null)
                ne.descr = "Some error occurred please try again after sometime"
            showSnack(placeholder, ne.descr)
        }
        catch (e: Exception){
            Utils.logException(throwable)
            showSnack(placeholder, "Some error occurred, please try again after sometime.")
        }

    }

    private fun getErrorBody(t: Throwable): NetworkError {
        val e = NetworkError()
        if (t is SocketTimeoutException) {
            e.name = NetworkError.SOCKET_TIMEOUT
            e.descr = "Connection Timed out, please check your internet connection and try again."
        } else if(t is HttpException){
            val r = t.response().errorBody()
            var err = errorResult(r)
            if(err==null || err.descr==null){
                when(t.code()){
                    500 -> e.descr = "Some problem occurred on the server, please try again after sometime"
                    else -> t.message
                }
                e.code = t.code()
            }
            else
                return err
        }
        else {
            e.name = t.message
            e.descr = t.localizedMessage
        }
        return e
    }

    private fun errorResult(errorBody: ResponseBody?) :  NetworkError?{
        val converter = RetrofitClient.getClient(Utils.getBaseUrl(getContext()), getContext())
                .responseBodyConverter<NetworkError>(NetworkError::class.java, arrayOfNulls(0))
        return try {
            if(errorBody!=null){
                val err: NetworkError = converter.convert(errorBody)
                err
            }
            else
                null
        } catch (e: IOException) {
            null
        }
    }


    override fun onLoadComplete(result: Any?, message: String) {
        if(message.isNotEmpty()) showSnack(findViewById(R.id.placeholder),message)
        val fr = getCurrentFragment()
        if(fr is OrderListFragment){
            if(result is OrderDetail){
                fr.notifyDataSetChanged(result)
            }
            else if(result is String){
                when(result){
                    Utils.RESULT_UPDATE_COMPLETE -> fr.notifyDataSetChanged(OrderDetail())
                    Utils.RESULT_SYNC_COMPLETE -> fr.checkUpdated()
                }
            }
        }

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                displayHomeFragment()
            }
            R.id.nav_analysis -> {
                replaceFragment(AnalysisFragment(),true)
            }
            R.id.nav_logout -> {
                logout()
            }
            R.id.nav_contact -> {
                contact()
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun contact() {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:+918826883595")
        startActivity(intent)
    }


    override fun saveToken(loginDetail: LoginDetail) {
        hideProgress()
        val sharedPrefs = getSharedPreferences(Utils.SHARED_PREFS, Context.MODE_PRIVATE)
        val editor = sharedPrefs.edit()
        NetworkRequest.loginDetail?.saveToPrefs(editor)
        editor.apply()
        successfulLogin(loginDetail)
    }

        override fun onRegistrationRequested() {
                replaceFragment(RegisterFragment(), true)
            }
    override fun onResume() {
        super.onResume()
        registerReceiver(notificationReceiver, IntentFilter(Utils.NOTIFICATION_INTENT))
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(notificationReceiver)
    }

    fun hideKeyboard(context: Context) {
        val imgr = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val v = (context as Activity).currentFocus ?: return
        imgr.hideSoftInputFromWindow(v.windowToken, 0)
    }
}
