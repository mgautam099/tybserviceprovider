package com.wetfiretech.tybserviceprovider;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class TybFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static final String TAG = "MyAndroidFCMIIDService";

    @Override
    public void onTokenRefresh() {
        //Get hold of the registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log the token
        saveRegistrationToServer(refreshedToken);
       // Log.d(TAG, "Refreshed token: " + refreshedToken);
    }
    private void saveRegistrationToServer(String token) {
        //Implement this method if you want to store the token on your server
        SharedPreferences sharedPrefs = getSharedPreferences(Utils.SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(Utils.KEY_REGISTRATION_TOKEN, token);
        editor.apply();

    }
}