package com.wetfiretech.tybserviceprovider.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wetfiretech.tybserviceprovider.custom.CircleView;
import com.wetfiretech.tybserviceprovider.R;
import com.wetfiretech.tybserviceprovider.fragment.OrderListFragment;
import com.wetfiretech.tybserviceprovider.model.OrderDetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderEntryAdapter extends RecyclerView.Adapter<OrderEntryAdapter.ViewHolder> {

    private final OrderListFragment.OnJobListInteractionListener mListener;
    private final List<OrderDetail> mValues;
    private SparseBooleanArray mHighlighted;

    public OrderEntryAdapter(List<OrderDetail> items, OrderListFragment.OnJobListInteractionListener listener) {
        mValues = items;
        mListener = listener;
        mHighlighted = new SparseBooleanArray();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order_entry, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).complain_no);
        holder.mProductView.setText(holder.mItem.product.name);
        if(holder.mItem.customer!=null){
            holder.mCustNameView.setText(holder.mItem.customer.name);
            holder.mPhoneNoView.setText(holder.mItem.customer.phone_no);
        }
        String[] date = holder.mItem.complain_date_time.substring(0,10).split("-");
        if(date.length==3){
            holder.mAddedOnView.setText(String.format("%s/%s/%s",date[2],date[1],date[0]));
        }
        else
            holder.mAddedOnView.setText("-/-/-");
        if(holder.mItem.end_service_provider!=null) {
            holder.mServiceView.setText(holder.mItem.end_service_provider.phone_no);
        }
        else
            holder.mServiceView.setText(" - ");
        holder.setStatus(holder.mItem.getOrderStatusEnum());
        Boolean b = mHighlighted.get(holder.mItem.eid);
        holder.mView.setActivated(b);
        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.onOrderClicked(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void highLightChanged(SparseBooleanArray highLightIndexes) {
        mHighlighted = highLightIndexes;
        notifyDataSetChanged();
    }

    public void highLightOrder(int order){
        mHighlighted.put(order,true);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mIdView;
        final TextView mCustNameView;
        final TextView mAddedOnView;
        final TextView mPhoneNoView;
        final TextView mProductView;
        final TextView mServiceView;
        final CircleView mStatusView;
        OrderDetail mItem;

        ViewHolder(View view) {
            super(view);
            mView = view.findViewById(R.id.rl_order_entry);
            mIdView = view.findViewById(R.id.tv_job_id);
            mProductView = view.findViewById(R.id.tv_product);
            mPhoneNoView = view.findViewById(R.id.tv_phone_no);
            mCustNameView = view.findViewById(R.id.tv_cust_name);
            mAddedOnView = view.findViewById(R.id.tv_added_on);
            mStatusView = view.findViewById(R.id.cv_order_status);
            mServiceView = view.findViewById(R.id.tv_service_provider);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mProductView.getText() + "'";
        }

        void setStatus(OrderDetail.OrderStatus orderStatus) {
            switch (orderStatus){
                case SERVICED: mStatusView.setColor(Color.GREEN); break;
                case ASSIGNED: mStatusView.setColor(Color.YELLOW); break;
                case REGISTERED: mStatusView.setColor(Color.RED); break;
            }
        }
    }
}
