package com.wetfiretech.tybserviceprovider.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.wetfiretech.tybserviceprovider.OnLoadFinishListener
import com.wetfiretech.tybserviceprovider.R
import com.wetfiretech.tybserviceprovider.Utils
import com.wetfiretech.tybserviceprovider.network.NetworkRequest
import kotlinx.android.synthetic.main.fragment_register.*
import kotlinx.android.synthetic.main.fragment_register.view.*

class RegisterFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_register, container, false)

        //This lambda checks for action from keyboard enter button
        view.et_partner_code.setOnEditorActionListener({ v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO)
                checkRegister(v)
            actionId == EditorInfo.IME_ACTION_GO
        })
        view.buttonLogin.setOnClickListener(this::checkRegister)
        return view
    }

    private fun checkRegister(v: View) {
        val username = etUsername.text.toString()
        val password = et_password.text.toString()
        val phone = etPhone.text.toString()
        val name = et_name.text.toString()
        val partnerCode = et_partner_code.text.toString()
        var isError = false
        isError = isError || validateET(tv_pass_error, password.isEmpty())
        isError = isError || validateET(tv_username_error, username.isEmpty())
        isError = isError || validateET(tv_name_error, name.isEmpty())
        isError = isError || validateET(tv_partner_error, partnerCode.isEmpty())
        isError = isError || validateET(tv_phone_error, !Utils.checkValidPhone(phone))
        if (!isError) {
            val registerDetails = HashMap<String, String>()
            registerDetails.put(Utils.KEY_USERNAME, username)
            registerDetails.put(Utils.KEY_PASSWORD, password)
            registerDetails.put(Utils.KEY_PHONE, phone)
            registerDetails.put(Utils.KEY_NAME, name)
            registerDetails.put(Utils.KEY_PARTNER_CODE, partnerCode)
            register(registerDetails)
        }
    }

    private fun validateET(tv: TextView, x: Boolean): Boolean {
        tv.visibility = if (x) View.VISIBLE else View.INVISIBLE
        return x
    }

    private fun register(registerDetails: HashMap<String, String>) {
        val sharedPreferences = context.getSharedPreferences(Utils.SHARED_PREFS, Context.MODE_PRIVATE)
        val registrationToken = sharedPreferences.getString(Utils.KEY_REGISTRATION_TOKEN, "")
        registerDetails.put(Utils.KEY_REGISTRATION, registrationToken)
        NetworkRequest.register(registerDetails, activity as OnLoadFinishListener)
    }

}