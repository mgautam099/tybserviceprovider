package com.wetfiretech.tybserviceprovider.fragment


import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.activeandroid.query.Select

import com.wetfiretech.tybserviceprovider.R
import com.wetfiretech.tybserviceprovider.model.OrderDetail


/**
 * A simple [Fragment] subclass.
 */
class AnalysisFragment : Fragment() {

    private val countOrderTotal by lazy{
        Select().from(OrderDetail::class.java).count()
    }
    private val countOrderRegistered by lazy{
        Select().from(OrderDetail::class.java).where("order_status = ?", OrderDetail.OrderStatus.REGISTERED).count()
    }

    private val countOrderAssigned by lazy{
        Select().from(OrderDetail::class.java).where("order_status = ?", OrderDetail.OrderStatus.ASSIGNED).count()
    }

    private val countOrderServiced by lazy{
        Select().from(OrderDetail::class.java).where("order_status = ?", OrderDetail.OrderStatus.SERVICED).count()
    }
//    private var mListener: OnProfileListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater!!.inflate(R.layout.fragment_analysis, container, false)
        view.findViewById<TextView>(R.id.tv_order_count_total).text = ""+countOrderTotal
        view.findViewById<TextView>(R.id.tv_order_count_reg).text = ""+countOrderRegistered
        view.findViewById<TextView>(R.id.tv_order_count_assign).text = ""+countOrderAssigned
        view.findViewById<TextView>(R.id.tv_order_count_serviced).text = ""+countOrderServiced
        return view
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AnalysisFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): AnalysisFragment {
            val fragment = AnalysisFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}