package com.wetfiretech.tybserviceprovider.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.wetfiretech.tybserviceprovider.R
import com.wetfiretech.tybserviceprovider.model.*
import kotlinx.android.synthetic.main.fragment_complain_detail.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [OrderEntryFragment.OnInteractionListener] interface
 * to handle interaction events.
 * Use the [OrderEntryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OrderEntryFragment : Fragment(){

    private var mListener: OnInteractionListener? = null

    private lateinit var orderDetail: OrderDetail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(savedInstanceState!=null){
            orderDetail = savedInstanceState.getParcelable(ARGS_ORDER)
        }
        else if( arguments!=null){
            orderDetail = arguments.getParcelable(ARGS_ORDER)
        }
        else
            orderDetail = OrderDetail()

        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_complain_detail,container,false)
        view.findViewById<View>(R.id.fab_save).setOnClickListener{ onSaveButtonPressed()}
        restore(view)
        return view
    }

    private fun onSaveButtonPressed() {
        if(validate()){
            if(orderDetail.end_service_provider==null){
                orderDetail.end_service_provider = EndServiceProvider()
            }
            var eserviceProvider: EndServiceProvider? = EndServiceProvider.getByPhone(et_sp_phone.text.toString())
            if(eserviceProvider == null) {
                eserviceProvider = EndServiceProvider()
                eserviceProvider.eid= -1
                eserviceProvider.phone_no = et_sp_phone.text.toString()
                eserviceProvider.save()
            }
            orderDetail.end_service_provider = eserviceProvider
            if( orderDetail.orderStatusEnum == OrderDetail.OrderStatus.REGISTERED){
                orderDetail.orderStatusEnum = OrderDetail.OrderStatus.ASSIGNED
            }
            else{
                orderDetail.orderStatusEnum = OrderDetail.OrderStatus.SERVICED
                if(!et_amount_paid.text.isNullOrEmpty())
                {
                    orderDetail.paidAmount = et_amount_paid.text.toString().toDouble()
                }
                if(!et_complain_comment.text.isNullOrEmpty())
                {
                    orderDetail.complain_resolve_comment = et_complain_comment.text.toString();
                }

            }
            orderDetail.updateType = OrderDetail.OrderUpdateType.PUT
            mListener?.onSaveRequested(orderDetail)
        }
    }

    fun restore(view: View){
        val c = orderDetail.customer
        view.findViewById<EditText>(R.id.et_cust_name).setText(c?.name)
        view.findViewById<EditText>(R.id.et_cust_phone).setText(c?.phone_no)
                view.findViewById<EditText>(R.id.et_product_name).setText(orderDetail.product?.name)
        view.findViewById<EditText>(R.id.et_address).setText(c?.address)
//        if(orderDetail.customer?.state == null && orderDetail.customer?.stateIndex != null && orderDetail.customer?.stateIndex !=1){
//            orderDetail.customer?.state = resources.getStringArray(R.array.states)[orderDetail.customer?.stateIndex!!]
//        }
        view.findViewById<EditText>(R.id.tv_state).setText(orderDetail.customer?.state?.stateName)
        view.findViewById<EditText>(R.id.tv_pin).setText(c?.pincode?.pincode)
        view.findViewById<EditText>(R.id.et_issue_description).setText(orderDetail.issue_description)
        view.findViewById<EditText>(R.id.et_sp_phone).setText(orderDetail.end_service_provider?.phone_no)
        lockFields(view,false)
        if(orderDetail.orderStatusEnum != OrderDetail.OrderStatus.REGISTERED) {
            view.findViewById<EditText>(R.id.et_complain_comment).setText(orderDetail.complain_resolve_comment)
            view.findViewById<EditText>(R.id.et_amount_paid).setText(orderDetail.paidAmount.toString())
        }
        else{
            view.findViewById<View>(R.id.til_complain_comment).visibility = View.GONE
            view.findViewById<View>(R.id.til_amount_paid).visibility = View.GONE
        }

        if(orderDetail.orderStatusEnum == OrderDetail.OrderStatus.SERVICED){
            view.findViewById<EditText>(R.id.et_complain_comment).isEnabled = false
            view.findViewById<EditText>(R.id.et_amount_paid).isEnabled = false
            view.findViewById<EditText>(R.id.et_sp_phone).isEnabled = false
            view.findViewById<View>(R.id.fab_save).visibility = View.GONE
        }
        else {
            view.findViewById<View>(R.id.fab_save).visibility = View.VISIBLE
        }
    }

    fun lockFields(view: View,islocked: Boolean){
        view.findViewById<EditText>(R.id.et_cust_phone).isEnabled = islocked;
        view.findViewById<EditText>(R.id.et_cust_name).isEnabled = islocked;
        view.findViewById<EditText>(R.id.et_address).isEnabled = islocked;
        view.findViewById<EditText>(R.id.tv_pin).isEnabled = islocked;
        view.findViewById<EditText>(R.id.tv_state).isEnabled = islocked;
        //rg_warranty.isEnabled=islocked;
        view.findViewById<EditText>(R.id.et_product_name).isEnabled=islocked;
        view.findViewById<EditText>(R.id.et_issue_description).isEnabled = islocked;
//        if()
    }

    private fun validate(): Boolean {
        var toReturn = true
        var text = et_sp_phone.text
        if(text.isEmpty() || (
                !text.startsWith("9") &&
                !text.startsWith("8") &&
                !text.startsWith("7") &&
                !text.startsWith("6") &&
                !text.startsWith("0")) ||
                text.length < 10){
            et_sp_phone.error = "Enter a valid Service Provider Number"
            toReturn = false
        }
        return toReturn
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }


    interface OnInteractionListener {
        fun onSaveRequested(orderDetail: OrderDetail)
    }

    fun isDirty(): Boolean{
        if(orderDetail.orderStatusEnum== OrderDetail.OrderStatus.SERVICED)
            return false
        if (!et_sp_phone.text.isEmpty())
            return true
        if(!et_amount_paid.text.isEmpty())
            return true
        if(!et_complain_comment.text.isEmpty())
            return true
        return false
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(ARGS_ORDER, orderDetail)
    }

    companion object {
        private val ARGS_ORDER = "order"

        fun newInstance(orderDetail: OrderDetail): OrderEntryFragment{
            val args = Bundle()
            val orderEntryFragment = OrderEntryFragment()
            args.putParcelable(ARGS_ORDER, orderDetail)
            orderEntryFragment.arguments = args
            return orderEntryFragment
        }
    }

}
